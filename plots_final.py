import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import class_readdata as rd
import matrix_calculation as matcal
import matrix_impactadv as matimp
import matrix_support as matsup
import scenarios_historic as scehis
import scenarios_future as scefut
import scenarios_buckets as scebuc
import ship_database as shpdtb

DPI = 600

def main():
    folders = matsup.get_folder_structure()
    ardapath = folders['arda']
    modelpath = folders['model']
    scenariopath = folders['scenarios']
    resultspath = folders['results']
    savepath = folders['figs']
    
    sns.set_style('ticks')
    sns.set_context('poster', font_scale=1.1)
    
    data = rd.ReadData(folders['model'])
    
    ldt_regression_plots(modelpath, savepath)
    aux_regression_plots(modelpath, savepath)
    historic_size_segment_percentages(savepath, data.seaweb)
    future_size_segment_percentages(savepath)   # font_scale=1.1
    lifetime_distribution(savepath)
    historic_and_future_economic_development(scenariopath, savepath)
    transport_work_prediction_models(scenariopath, savepath)
    scenarios_transport_work_predictions(scenariopath, savepath)
    ship_characteristics_bucketwise_all_years(savepath, data.seaweb)   # font_scale=1.5
    advanced_contribution_calculation_2016(ardapath, resultspath, savepath, data.seaweb)
    advanced_contribution_plots(resultspath, savepath)   # font_scale=1.4
    size_bin_distribution_current(resultspath, savepath, data.seaweb)   # font_scale=1.1
    scenarios_total_gwp_bucket_wise(resultspath, savepath)   # font_scale=1.1
    

def ldt_regression_plots(loadpath, savepath):
    ldt_df = import_regression_data(loadpath)
    
    plot_regression_against_ldt('denim', 'GT', 'Gross tonnage (thousand tonnes)',
                                ldt_df, savepath, [0, 50, 100, 150, 200], 'off')
    plot_regression_against_ldt('avocado', 'Deadweight', 'Deadweight (thousand tonnes)',
                                ldt_df, savepath, [0, 50, 100, 150, 200], 'off')
    plot_regression_against_ldt('aquamarine', 'TEU', 'Twenty-foot equivalent units',
                                ldt_df, savepath, xticks='off')
    plot_regression_against_ldt('seaweed', 'Length_BP', 'Length (m)', ldt_df, savepath)
    plot_regression_against_ldt('sky blue', 'Breadth', 'Breadth (m)', ldt_df, savepath)
    plot_regression_against_ldt('green grey', 'Box', 'Box (length x breadth x draught)',
                                ldt_df, savepath)

def aux_regression_plots(loadpath, savepath):
    main_df = import_regression_data(loadpath)
    global aux_df
    aux_df = shpdtb.add_aux_eng_values(main_df, loadpath)
    aux_df = aux_df[aux_df.AuxEng < 10000]
    
    plot_regression_against_aux('denim', 'GT', 'Gross tonnage (thousand tonnes)',
                                aux_df, savepath, [0, 50, 100, 150, 200], 'off')
    plot_regression_against_aux('avocado', 'Deadweight', 'Deadweight (thousand tonnes)',
                                aux_df, savepath, [0, 50, 100, 150, 200], 'off')
    plot_regression_against_aux('aquamarine', 'TEU', 'Twenty-foot equivalent units',
                                aux_df, savepath, xticks='off')
    plot_regression_against_aux('seaweed', 'Length_BP', 'Length (m)', aux_df, savepath)
    plot_regression_against_aux('sky blue', 'Breadth', 'Breadth (m)', aux_df, savepath)
    plot_regression_against_aux('green grey', 'Total_KW_Main_Eng', 'Main engine capacity (kW)',
                                aux_df, savepath)

def import_regression_data(loadpath):
    ##Importing the data from Excel and set the IMO no. as the index
    sw_filename = loadpath + 'SeawebContainerAllHistoric.xlsx'
    sw_raw = pd.read_excel(sw_filename, na_values=0)
    sw_raw.set_index('LR/IMO_Ship_No.', inplace=True)
    
    ##Filtering out only the ships that are in use today
    inservice = sw_raw.Status == 'In Service/Commission'
    sw_inservice = sw_raw.loc[inservice,:]
    
    ### Only keep the building year in the Built column
    sw_inservice.loc[:,'Built'] = [int(i[:-3]) for i in sw_inservice.Built]
    
    ##Create dataframe where all NaN-entries of the LDT-column are excluded
    sw_ldt = sw_raw[sw_raw.LDT.notnull()]
    
    ##There are enough ships "In Service/Commission" that it makes sense to use
    ##only these for regression to fill in LDT values for other In Service ships
    ldt_inservice = sw_ldt['Status'] == 'In Service/Commission'
    ldt = sw_ldt.loc[ldt_inservice,:]
    
    ##Creating a new column with the Box volume of each ship
    ##NB! This is not the box coefficient, just the the dimensions multiplied
    ldt = ldt.assign(Box=ldt['Length_BP']*ldt['Breadth']*ldt['Draught'])
    
    return ldt

def plot_regression_against_ldt(xkcd_color, var_col, var_name, ldt_df, 
                                savepath, ylabl=[], xticks='on'):
    sns.set_palette(sns.xkcd_palette([xkcd_color]))
    plt.figure()
    ldt_var = sns.lmplot('LDT', var_col, ldt_df, scatter_kws={'s':50})
    plt.xlim(xmin=0)
    plt.ylim(ymin=0)
    if xticks == 'off':
        ldt_var.set(xticklabels=[])
        plt.xlabel('')
    else:
        plt.xlabel('Light displacement tonnage')
    if len(ylabl) > 0:
        ldt_var.set(yticklabels=ylabl)
    plt.ylabel(var_name)
    plt.savefig(savepath + 'LDT_' + var_col + '.png', dpi=DPI)

def plot_regression_against_aux(xkcd_color, var_col, var_name, aux_df, 
                                savepath, ylabl=[], xticks='on'):
    sns.set_palette(sns.xkcd_palette([xkcd_color]))
    plt.figure()
    ldt_var = sns.lmplot('AuxEng', var_col, aux_df, fit_reg=False, scatter_kws={'s':50})
    plt.xlim(xmin=0)
    plt.ylim(ymin=0)
    if xticks == 'off':
        ldt_var.set(xticklabels=[])
        plt.xlabel('')
    else:
        plt.xlabel('Auxiliary engine capacity (kW)')
    if len(ylabl) > 0:
        ldt_var.set(yticklabels=ylabl)
    plt.ylabel(var_name)
    plt.savefig(savepath + 'aux_' + var_col + '.png', dpi=DPI)

def historic_size_segment_percentages(savepath, seaweb_data):
    sns.set_context('poster')
    palette = get_marine_palette_list()
    
    segment_data = scebuc.size_and_year_segments(seaweb_data)
    buckets = scebuc.get_size_buckets_names()
    plot_names = scebuc.get_size_buckets_plot_names()
    
    for i in range(len(segment_data.bucket.unique())):
        sns.set_palette(sns.xkcd_palette([palette[i]]))
        plt.figure()
        segment = segment_data[segment_data.bucket == buckets[i]]
        sns.lmplot('index', 'percentage', segment.reset_index())
        plt.xlabel('Year')
        plt.ylabel('Percent of fleet')
        plt.ylim(ymin=0, ymax=40)
        plt.yticks(np.arange(0,5) * 10)
        plt.title(plot_names[i])
        if i%3 != 0:
            plt.tick_params(labelleft='off')
            plt.ylabel('')
        if i in [j for j in np.arange(len(buckets))[:6]]:
            plt.tick_params(labelbottom='off')
            plt.xlabel('')
        plt.savefig(savepath + 'percent_' + buckets[i] + '.png', dpi=DPI, bbox_inches='tight')

def future_size_segment_percentages(savepath):
    palette = get_marine_palette_list()[::-1]
    sns.set_palette(sns.xkcd_palette(palette))
    
    dist = scebuc.create_future_segments() * 100
    plt.figure()
    dist.index = scebuc.get_size_buckets_plot_names()
    dist.T.plot.area().legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel('Year')
    plt.ylabel('Percent of fleet')
    plt.ylim(ymax=100)
    plt.savefig(savepath + 'future_buckets.png', dpi=DPI, bbox_inches='tight')

def lifetime_distribution(savepath):
    sns.set_palette(sns.xkcd_palette(['denim']))
    from scipy.stats import norm
    
    mu = 25
    sigma = 5
    
    plt.figure()
    x_axis = np.linspace(mu-3*sigma, mu+3*sigma, 100)
    plt.plot(x_axis, norm.pdf(x_axis, mu, sigma))
    plt.xlabel('Age of ship')
    plt.ylabel('Probability of disposal')
    plt.ylim(ymin=0)
    plt.savefig(savepath + 'lifetimedist.png', dpi=DPI)

def historic_and_future_economic_development(loadpath, savepath):
    palette = get_marine_palette_list()[3:]
    palette.insert(0, 'black')
    sns.set_palette(sns.xkcd_palette(palette))
    plt.figure()
    gdp, pop, tkm = scehis.import_scenario_training_data(loadpath)
    gdp_cap_his = gdp * 1000 / pop
    gdp_cap_fut = scehis.import_scenario_future_data(loadpath)
    gdp_cap_all = pd.concat([gdp_cap_his, gdp_cap_fut.T], axis=1)
    gdp_cap_all.columns = ['Historic', 
                           'SSP1', 
                           'SSP2', 
                           'SSP3', 
                           'SSP4', 
                           'SSP5']
    
    gdp_cap_all.plot()
    plt.xlabel('Year')
    plt.xlim(xmin=1975)
    plt.ylabel('GDP per capita (US$2005/cap)')
    plt.ylim(ymin=0)
    plt.axvline(x=2050, linewidth=1)
    plt.savefig(savepath + 'historic_and_future_gdp_per_cap.png', dpi=DPI)

def transport_work_prediction_models(loadpath, savepath):
    palette = get_marine_palette_list()
    palette.insert(3, 'black')
    sns.set_palette(sns.xkcd_palette(palette))
    plt.figure()
    
    gdp, pop, tkm = scehis.import_scenario_training_data(loadpath)
    gdp_fut, pop_fut = scehis.import_scenario_future_data(loadpath, 'training')
    
    mask_all = gdp.notnull() & pop.notnull() & tkm.notnull()
    mask_list = mask_all[mask_all].index.tolist()
    
    gdp_00_10 = scehis.fill_values_ssp_data(gdp, mask_list[0], mask_list[-1])
    pop_00_10 = scehis.fill_values_ssp_data(pop, mask_list[0], mask_list[-1])
    
    gdp_10_20 = scehis.fill_values_ssp_data(gdp_fut, 2010, 2020)
    pop_10_20 = scehis.fill_values_ssp_data(pop_fut, 2010, 2020)
    
    gdp_per_cap_00_10 = gdp_00_10 * 1000 / pop_00_10   # unit is now US$2005/capita
    gdp_per_cap_10_20 = gdp_10_20 * 1000 / pop_10_20
    gdp_per_cap = gdp_per_cap_00_10.append(gdp_per_cap_10_20)
    
    mask_tkm = tkm.notnull() & gdp_per_cap.notnull()
    tkm_00_16 = tkm[mask_tkm[mask_tkm].index]
    
    pred = gdp_per_cap[mask_tkm]
    resp = tkm_00_16
    
    linear = np.polyfit(pred, resp, 1)
    quadratic = np.polyfit(pred, resp, 2)
    logarithmic = np.polyfit(np.log(pred), resp, 1)
    
    # Creating an x-axis to plot the functions against
    x = np.linspace(np.amin(pred), np.amax(pred)*2)
    
    # Overlaying a scatter plot of the data on the relationship functions
    plt.plot(x, np.polyval(linear, x), label='Linear model')
    plt.plot(x, np.polyval(quadratic, x), label='Quadratic model')
    plt.plot(x, np.polyval(logarithmic, np.log(x)), label='Logarithmic model')
    plt.plot(pred, resp, marker='.', linestyle='none', label='Training data')
    
    # Adding axis labels and legend
    plt.xlabel('GDP per capita (US$2005)')
    plt.ylabel('Transport work (tkm)')
    plt.ylim(ymin=0)
    plt.legend()
    
    plt.savefig(savepath + 'tkm_training' + '.png', dpi=DPI)

def scenarios_transport_work_predictions(loadpath, savepath):
    palette = get_marine_palette_list()[3:]
    sns.set_palette(sns.xkcd_palette(palette))
    plt.figure()
    
    gdp_cap = scehis.import_scenario_future_data(loadpath)
    
    future_tkm_log = scefut.compute_future_transport_work(loadpath, 'logarithmic', gdp_cap)
    future_tkm_log.index.name = None
    
    plt.figure(); future_tkm_log.transpose().plot()
    plt.xlabel('Year')
    plt.xlim(xmax=2050)
    plt.ylabel('Transport work (tkm)')
    plt.ylim(ymin=0, ymax=5E+13)
    plt.savefig(savepath + 'tkm_prediciton_log.png', dpi=DPI)

def ship_characteristics_bucketwise_all_years(savepath, seaweb_data):
    palette = get_marine_palette_list()[::-1]
    sns.set_palette(sns.xkcd_palette(palette))
    
    bucket_year_means = seaweb_data.groupby(['Built', 'bucket']).mean()
    buckets = scebuc.get_size_buckets_names()
    plot_names = scebuc.get_size_buckets_plot_names()
    
    prop_list = ['DWT', 'TEU', 'GT', 'LDT', 'MainEng', 'AuxEng']
    plot_titles = ['Deadweight', 'Twenty-foot equivalent units', 'Gross tonnage',
                  'Light displacement tonnage', 'Main engine total kW',
                  'Auxiliary engine total kW']
    
    for prop in prop_list:
        plt.figure()
        temp = bucket_year_means[prop].unstack()
        temp = temp.reindex_axis(buckets, axis=1)
        temp = temp.rename(columns=dict(zip(buckets, plot_names)))
        temp.columns.name = None
        if prop == 'GT':
            temp.plot()
        else:
            temp.plot(legend=None)
        plt.xlabel('Year')
        plt.ylabel(plot_titles[prop_list.index(prop)])
        plt.ylim(ymin=0)
        plt.axvline(x=2016, color='k', linewidth=1)
        plt.savefig(savepath + 'char_' + prop + '.png', dpi=DPI, bbox_inches='tight')

def advanced_contribution_calculation_2016(ardapath, resultspath, savepath, seaweb_data):
    y_all = matsup.pickle_load(ardapath + 'y_ALL.dat')
    y_single = y_all['2016']
    
    matrices_all = matimp.import_matrices(ardapath, 2016, y_single)
    d_adv_all = matcal.advanced_contribution_analysis(matrices_all)
    d_adv_all.to_excel(resultspath + 'adv_all.xlsx', sheet_name='all')
    
    size_names = scebuc.get_size_buckets_names()
    
    for bucket in size_names:
        y_sub = y_single[(seaweb_data.bucket == bucket) & (y_single > 0)]
        matrices = matimp.import_matrices(ardapath, 2016, y_sub)
        d_adv = matcal.advanced_contribution_analysis(matrices)
        d_adv.to_pickle(resultspath + 'adv_' + bucket)
        d_adv.to_excel(resultspath + 'adv_' + bucket + '.xlsx', sheet_name=bucket)

def advanced_contribution_plots(loadpath, savepath):
    palette = get_marine_palette_list_adv()
    sns.set_palette(sns.xkcd_palette(palette))
    plt.figure()
    
    size_names = scebuc.get_size_buckets_names()
    plot_names = scebuc.get_size_buckets_plot_names()
    
    for bucket in size_names:
        d_adv = pd.read_pickle(loadpath + 'adv_' + bucket)
        matimp.advanced_contribution_plot(d_adv, savepath, 
                                          plot_names[size_names.index(bucket)])

def size_bin_distribution_current(loadpath, savepath, seaweb_data):
    palette = get_marine_palette_list()[::-1]
    sns.set_palette(sns.xkcd_palette(palette))
    plt.figure()
    
    results = pd.read_pickle(loadpath + 'SSP1')
    sixteen = results.xs(2016, level='year', axis=1)
    gwp_abs = sixteen.loc[sixteen.index.str.startswith('GWP')]
    gwp_abs = gwp_abs.rename(index={'GWP100/Hierarchist/kg CO2 eq':'GWP100 (kg CO2 eq)'})
    gwp_abs.columns.name = None
    gwp_rel = gwp_abs.div(gwp_abs.sum(axis=1), axis=0) * 100
    physic = calculate_fleet_proportions(seaweb_data)
    df_toplot = pd.concat([gwp_rel, physic])
    df_toplot.columns = scebuc.get_size_buckets_plot_names()
    df_toplot.plot(kind='bar', stacked=True, rot=0).legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.ylim(ymin=0, ymax=100)
    plt.ylabel('Relative to fleet total (%)')
    plt.savefig(savepath + 'twentysixteen.png', dpi=DPI, bbox_inches='tight')

def scenarios_total_gwp_bucket_wise(loadpath, savepath):
    palette = get_marine_palette_list()[::-1]
    sns.set_palette(sns.xkcd_palette(palette))
    
    buckets = scebuc.get_size_buckets_names()
    plot_names = scebuc.get_size_buckets_plot_names()
    
    for ssp in ['SSP1', 'SSP2', 'SSP3', 'SSP4', 'SSP5']:
        # Preparing Data Frame
        df = pd.read_pickle(loadpath + ssp)
        gwp = df.loc[df.index.str.startswith('GWP')]
        gwp_df = gwp.T.unstack()
        gwp_df.index.name = None
        gwp_df = gwp_df.reindex(buckets)
        gwp_df = gwp_df.rename(dict(zip(buckets, plot_names)))
        gwp_df.columns = gwp_df.columns.droplevel()
        # Fixing the plot
        plt.figure()
        gwp_df.T.plot.area().legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.title(ssp)
        plt.xlabel('Year')
        plt.ylabel('Global Warming Potential (CO$_2$ eq)')
        plt.ylim(ymax = 2.5E+11)
        plt.yticks(np.arange(0,3) * 1E+11)
        plt.savefig(savepath + ssp + '.png', dpi=DPI, bbox_inches='tight')

def get_marine_palette_list():
    palette = ['denim', 
               'grey pink', 
               'green grey', 
               'sky blue', 
               'ugly yellow',
               'blue green', 
               'olive', 
               'light indigo', 
               'azure']
    return palette

def get_marine_palette_list_adv():
    palette = ['avocado',
               'denim', 
               'aquamarine', 
               'black', 
               'green grey']
    return palette

def calculate_fleet_proportions(seaweb_data):
    sizes = scebuc.get_size_buckets_names()
    df = pd.DataFrame(index=['Number of ships', 'Capacity of ships (TEU)'], columns=sizes)
    
    total_number = seaweb_data.Name.count()
    total_teu = seaweb_data.TEU.sum()
    
    for z in sizes:
        subset = seaweb_data[seaweb_data.bucket == z]
        df.loc['Number of ships', z] = subset.Name.count() * 100 / total_number
        df.loc['Capacity of ships (TEU)', z] = subset.TEU.sum() * 100 / total_teu
    
    return df


if __name__ == '__main__':
    main()