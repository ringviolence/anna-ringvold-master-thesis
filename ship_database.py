import os
import pandas as pd
import numpy as np

import scenarios_buckets as scebuc
import matrix_support as matsup

def clean_seaweb_dataset(path):
    os.chdir(path)
    sw_raw = import_seaweb_data()
    sw_in = filter_out_ships_not_in_use(sw_raw)
    sw = rename_columns(sw_in)
    sw = only_keep_building_year(sw)
    sw = add_aux_eng_values(sw, path)
    sw = add_box_volume_column(sw)
    sw = fill_in_missing_values(sw)

    want_cols = ['Name', 'Built', 'DWT', 'TEU', 'GT', 'LDT', 'MainEng', 'AuxEng']
    seaweb = sw.loc[:, want_cols]
    
    years = np.arange(2017,2051)
    size_buckets = scebuc.get_size_buckets()
    size_names = scebuc.get_size_buckets_names()

    seaweb = add_future_ships(seaweb, years, size_buckets, size_names)
    seaweb = add_time_spent_in_each_trip_phase(seaweb)
    seaweb = add_size_buckets(seaweb, size_buckets, size_names)
    seaweb = add_net_payload_percentage(seaweb, size_names)
    
    return seaweb

def import_seaweb_data():
    sw_filename = 'SeawebContainerAllHistoric.xlsx'
    return pd.read_excel(sw_filename, index_col=0, na_values=0)

def filter_out_ships_not_in_use(raw_df):
    return raw_df[raw_df.Status == 'In Service/Commission']

def rename_columns(df):
    cols = {'Name_of_Ship':'Name', 
            'Deadweight':'DWT', 
            'Total_KW_Main_Eng':'MainEng'}
    return df.rename(columns=cols)

def only_keep_building_year(df):
    df.loc[:,'Built'] = [int(i[:-3]) for i in df.Built]
    return df

def add_aux_eng_values(df, loadpath):
    aux_filename = 'SeawebContainerAuxEngine.xlsx'
    aux_raw = pd.read_excel(loadpath + aux_filename, na_values=0)
    aux_raw = aux_raw.set_index('LR/IMO_Ship_No.')
    aux_raw.rename(columns={'Aux._Engine_Total_KW':'AuxEng'}, inplace=True)
    result = pd.concat([df, aux_raw.AuxEng], axis=1, join_axes=[df.index])
    return result

def add_box_volume_column(df):
    return df.assign(Box=df['Length_BP']*df['Breadth']*df['Draught'])

def fill_in_missing_values(df):
    mbox1 = df.LDT.notnull() & df.Box.notnull()
    box_ldt = np.polyfit(df.Box[mbox1], df.LDT[mbox1], 1)
    mbox2 = df.LDT.isnull() & df.Box.notnull()
    df.loc[mbox2, 'LDT'] = np.polyval(box_ldt, df.Box[mbox2])
    
    mdwt1 = df.LDT.notnull() & df.DWT.notnull()
    dwt_ldt = np.polyfit(df.DWT[mdwt1], df.LDT[mdwt1], 1)
    mdwt2 = df.LDT.isnull()
    df.loc[mdwt2, 'LDT'] = np.polyval(dwt_ldt, df.DWT[mdwt2])
    
    mmen1 = df.LDT.notnull() & df.MainEng.notnull()
    dwt_meng = np.polyfit(df.DWT[mmen1], df.MainEng[mmen1], 1)
    mmen2 = df.MainEng.isnull()
    df.loc[mmen2, 'MainEng'] = np.polyval(dwt_meng, df.DWT[mmen2])
    
    mteu1 = df.LDT.notnull() & df.TEU.notnull()
    dwt_teu = np.polyfit(df.DWT[mteu1], df.TEU[mteu1], 2)
    mteu2 = df.TEU.isnull()
    df.loc[mteu2, 'TEU'] = np.polyval(dwt_teu, df.DWT[mteu2])
    
    maux1 = df.AuxEng.notnull() & (df.AuxEng < 10000) & df.DWT.notnull()
    dwt_aux = np.polyfit(df.DWT[maux1], df.AuxEng[maux1], 2)
    maux2 = df.AuxEng.isnull()
    df.loc[maux2, 'AuxEng'] = np.polyval(dwt_aux, df.DWT[maux2])
    
    return df

def add_future_ships(df, years_ls, buckets, names):
    
    for year in years_ls:
        for i in range(len(buckets)-1):
            mask = (df.TEU > buckets[i]) & (df.TEU <= buckets[i+1])
            mask_15y = (df.Built >= year-15)
            mask_3y = (df.Built >= year-3)
            s = pd.Series()
            IMO = 'F' + str(year) + '_' + names[i]
            s['Name'] = 'FUTURE SHIP ' + names[i] + ' ' + str(year)
            s['Built'] = year
            for mean in ['DWT', 'TEU', 'GT', 'LDT', 'AuxEng']:
                s[mean] = df.loc[mask & mask_15y, mean].mean()
            s['MainEng'] = df.loc[mask & mask_3y, 'MainEng'].mean()
            df.loc[IMO] = s
    
    return df

def add_time_spent_in_each_trip_phase(df):
    time_filename = 'lindstad_time.csv'
    need_columns = ['TEU', 'Cruise_h', 'Manoeuvre_h', 'Hotel_h']
    time = pd.read_csv(time_filename, skiprows=9, index_col=0, usecols=need_columns)
    time = time.reindex(index=time.index[::-1])
    
    mask_below = df.TEU <= time.index[0]
    df.loc[mask_below,'cru'] = time.loc[time.index[0], 'Cruise_h']
    df.loc[mask_below,'man'] = time.loc[time.index[0], 'Manoeuvre_h']
    df.loc[mask_below,'hot'] = time.loc[time.index[0], 'Hotel_h']
    
    mask_above = df.TEU > time.index[-1]
    df.loc[mask_above,'cru'] = time.loc[time.index[-1], 'Cruise_h']
    df.loc[mask_above,'man'] = time.loc[time.index[-1], 'Manoeuvre_h']
    df.loc[mask_above,'hot'] = time.loc[time.index[-1], 'Hotel_h']
    
    for i in range(len(time.index)-1):
        mask = (df.TEU > time.index[i]) & (df.TEU <= time.index[i+1])
        df.loc[mask,'cru'] = time.loc[time.index[i], 'Cruise_h']
        df.loc[mask,'man'] = time.loc[time.index[i], 'Manoeuvre_h']
        df.loc[mask,'hot'] = time.loc[time.index[i], 'Hotel_h']
    
    return df

def add_size_buckets(df, buckets, names):
    for i in range(len(buckets)-1):
        mask = (df.TEU > buckets[i]) & (df.TEU <= buckets[i+1])
        df.loc[mask,'bucket'] = names[i]
    return df

def add_net_payload_percentage(df, names):
    for i in names[:2]:
        df.loc[df.bucket == i, 'payload'] = 0.75
    for i in names[2:]:
        df.loc[df.bucket == i, 'payload'] = 0.80
    
    return df


if __name__ == '__main__':
    folders = matsup.get_folder_structure()
    clean_seaweb_dataset(folders['model'])