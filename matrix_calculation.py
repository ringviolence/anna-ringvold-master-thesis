import numpy as np
import pandas as pd
from scipy import sparse

import matrix_support as matsup

def impact_calculation(L, F, C, y, imp_idx):
    """A simple impact calculation that takes four sparse arrays as input and
    returns the hierarchist results as a data frame. Input are sparse matrices.
    """
    x = L * y
    e = F * x
    d = C * e
    d_df = pd.DataFrame(d.todense(), index=imp_idx)
    d_hierarchist = d_df[['Hierarchist' in s for s in d_df.index]]
    return d_hierarchist

def advanced_contribution_analysis(matrix_list):
    """Advances contribution calculation that allocates all the background
    processes to its foreground process, so one can see which processes
    generate the greatest impact. Input are sparse matrices in a list in the
    following order: [A_bf, L_ff, L_bb, F_f, F_b, C, y_f, imp_idx].
    """
    A_bf = matrix_list[0]
    L_ff = matrix_list[1]
    L_bb = matrix_list[2]
    F_f = matrix_list[3]
    F_b = matrix_list[4]
    C = matrix_list[5]
    y_f = matrix_list[6]
    imp_idx = matrix_list[7]
    
    f_size = L_ff.shape[0]
    
    x_f = L_ff * y_f
    x_f_coo = x_f.tocoo()
    x_f_hat = sparse.csc_matrix((x_f_coo.data, (x_f_coo.row, x_f_coo.row)), 
                                shape=(f_size, f_size))
        
    D_pro_ff = C * F_f * x_f_hat
    D_pro_bf = C * F_b * L_bb * A_bf * x_f_hat
    D_pro_f = D_pro_ff + D_pro_bf
    
    D_pro_f_list = []
    
    pro_idx = matsup.get_foreground_flow_names()
    
    for i in range(len(pro_idx)):
        column = D_pro_f[:, np.arange(start=i, stop=f_size, step=len(pro_idx))].sum(axis=1)
        D_pro_f_list.append(column)
    
    D_pro_f_df = pd.DataFrame(np.hstack(D_pro_f_list), index=imp_idx, columns=pro_idx)
    D_pro_f_hierarchist = D_pro_f_df[['Hierarchist' in s for s in D_pro_f_df.index]]
    
    return D_pro_f_hierarchist