""" Class that reads in all the necessary background data upon init """

import os
import pandas as pd

import ship_database as shpdtb
import scenarios_buckets as scebuc

class ReadData:
    
    """ Loading the data required to run the LCA """
    
    def __init__(self, path):
        os.chdir(path)
        self.seaweb = shpdtb.clean_seaweb_dataset(path)
        self.ecoinv = pd.read_csv('ecoinvent_bulker.csv', skiprows=1, index_col=1)
        self.sarraf = pd.read_csv('sarraf_beaching.csv', skiprows=1, index_col=1)
        self.ardaid = pd.read_csv('ardaIDs.csv', index_col=0)
        self.ef_tr1 = pd.read_csv('eea_tier1.csv', index_col=1, encoding='iso-8859-1')
        self.ef_tr3 = pd.read_csv('eea_tier3.csv', index_col=1)
        self.tkminfo = scebuc.create_future_transport_work()