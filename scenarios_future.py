import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import matrix_support as matsup
import scenarios_historic as scehis


def main(error_calc=True):
    folders = matsup.get_folder_structure()
    os.chdir(folders['scenarios'])
    
    gdp_cap = scehis.import_scenario_future_data()
    
    linear = compute_future_transport_work('linear', gdp_cap)
    quadratic = compute_future_transport_work('quadratic', gdp_cap)
    logarithmic = compute_future_transport_work('logarithmic', gdp_cap)
    
    if error_calc == True:
        tkm_hist = scehis.get_regression_coefficients('tkm_data')
        ipl_lin = scehis.fill_values_ssp_data(linear.mean(0), 2010, 2020)
        ipl_quad = scehis.fill_values_ssp_data(quadratic.mean(0), 2010, 2020)
        ipl_log = scehis.fill_values_ssp_data(logarithmic.mean(0), 2010, 2020)
        err_dict = {'linear':calculate_percentage_error(ipl_lin, tkm_hist), 
                    'quadratic':calculate_percentage_error(ipl_quad, tkm_hist), 
                    'logarithmic':calculate_percentage_error(ipl_log, tkm_hist)}
        percentage_errors = pd.concat(err_dict, axis=1)
        print(percentage_errors.dropna())
    
    os.chdir(folders['main'])

def get_transport_work_for_future_years(loadpath, regr='logarithmic'):
    gdp_cap = scehis.import_scenario_future_data(loadpath)
    small = compute_future_transport_work(loadpath, regr, gdp_cap)
    large = pd.DataFrame(index=small.index, columns=range(2010,2101), dtype='float64')
    large[small.columns] = small
    large = large.interpolate(axis=1)
    
    return large.loc[:,:2050]

def compute_future_transport_work(loadpath, regr, gdp_cap_df, figure=False):
    coeffs = scehis.get_regression_coefficients(loadpath, regr)
    if regr == 'logarithmic':
        x = np.log(gdp_cap_df.values)
    else:
        x = gdp_cap_df.values
    tkm_future = np.polyval(coeffs, x)
    tkm_future_df = pd.DataFrame(tkm_future, index=gdp_cap_df.index, 
                                 columns=gdp_cap_df.columns)
    if figure == True:
        plt.figure(); tkm_future_df.transpose().plot()
    return tkm_future_df

def calculate_percentage_error(measurement, true_value):
    """Calculates the percentage error between predicted and historic ("true")
    values where there is overlap. Input can be single value or Series.
    """
    return (measurement - true_value) * 100 / true_value


if __name__ == '__main__':
    folders = matsup.get_folder_structure()
    get_transport_work_for_future_years(folders['scenarios'])
#    main()