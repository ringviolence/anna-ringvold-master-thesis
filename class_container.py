import class_support as hf

class Container:
    
    """ Creates a ship object that loads all necessary ship characteristics
        upon init. Own functions for calculating the flows and stressors for
        each of the ships's lifecycles. """
    
    def __init__(self, IMO, data):
        self.imo = IMO
        self.built = data.seaweb.Built[IMO]
        self.dwt = data.seaweb.DWT[IMO]
        self.teu = data.seaweb.TEU[IMO]
        self.gt = data.seaweb.GT[IMO]
        self.ldt = data.seaweb.LDT[IMO]
        self.eng_m = data.seaweb.MainEng[IMO]
        self.eng_a = data.seaweb.AuxEng[IMO]
        self.cru_h = data.seaweb.cru[IMO]
        self.man_h = data.seaweb.man[IMO]
        self.hot_h = data.seaweb.hot[IMO]
        self.size_bucket = data.seaweb.bucket[IMO]
        self.payload_percent = data.seaweb.payload[IMO]

    def calc_cnst(self, data):
        """Calculating the amount of construction materials and energy for ship
        amount = ldt of ship * ecoinvent value per ldt
        """
        ldt = self.ldt
        eco = data.ecoinv.PerMisc
        arda = data.ardaid
        # A matrix
        self.copper = hf.flowdict(ldt, 'copper', eco, arda)
        self.polyethylene = hf.flowdict(ldt, 'polyethylene', eco, arda)
        self.alkyd_paint = hf.flowdict(ldt, 'alkyd_paint', eco, arda)
        self.reinf_steel = hf.flowdict(ldt, 'reinforcing_steel', eco, arda)
        self.electricity = hf.flowdict(ldt, 'electricity', eco, arda)
        self.heat = hf.flowdict(ldt, 'heat', eco, arda)
    
    def calc_eol(self, data):
        """Calculating the amount of waste material and environmental stressors
        generated when beaching a ship. The values are from Sarraf et al (2010)
        World Bank report on the 'Shipbreaking and Recycling Industry in
        Bangladesh and Pakistan'. The first waste flow is from the Ecoinvent
        database and are calculated based on a ship's LDT, while the values
        from Sarraf et al are based on a ship's GT.
        """
        ldt = self.ldt
        eco = data.ecoinv.PerMisc
        gt = self.gt
        beach = data.sarraf.PerGT
        arda = data.ardaid
        # A matrix
        self.waste_plastic = hf.flowdict(ldt, 'w_plastic', eco, arda)
        self.waste_minoil = hf.flowdict(gt, 'w_mineraloil', beach, arda)
        self.waste_pufoam = hf.flowdict(gt, 'w_pufoam', beach, arda)
        self.waste_paint = hf.flowdict(gt, 'w_paint', beach, arda)
        self.wastewater = hf.flowdict(gt, 'wastewater', beach, arda)
        # F matrix
        self.pcb_air_eol = hf.flowdict(gt, 'PCBs_a', beach, arda)
        self.pcb_soil = hf.flowdict(gt, 'PCBs_s', beach, arda)
        self.pcb_water = hf.flowdict(gt, 'PCBs_w', beach, arda)
        self.pb_soil_ind = hf.flowdict(gt, 'Pb_s_industrial', beach, arda)
        self.pb_soil_usp = hf.flowdict(gt, 'Pb_s_unspecified', beach, arda)
        self.pb_water = hf.flowdict(gt, 'Pb_w', beach, arda)
        self.cd_soil = hf.flowdict(gt, 'Cd_s', beach, arda)
        self.cd_water = hf.flowdict(gt, 'Cd_w', beach, arda)
        self.hg_water = hf.flowdict(gt, 'Hg_w', beach, arda)
        self.hg_soil = hf.flowdict(gt, 'Hg_s', beach, arda)
        self.oils_soil = hf.flowdict(gt, 'Oils_s', beach, arda)
        self.oils_water = hf.flowdict(gt, 'Oils_w', beach, arda)
        self.hcl_water = hf.flowdict(gt, 'HCl_w', beach, arda)
    
    def calc_transport(self, data, current_year):
        tkm_data = data.tkminfo[current_year].xs(self.size_bucket)
        self.fuel = hf.total_fuelcons([self.cru_h, self.man_h, self.hot_h], 
                                      self.built, self.eng_m, self.eng_a, tkm_data.slow_steaming)   # tonnes
        self.tkm = tkm_data.load * self.dwt * self.payload_percent * tkm_data.trip_count * tkm_data.trip_km
        self.port_char = tkm_data.load * self.dwt * self.payload_percent * tkm_data.trip_count / 100
    
    def calc_use(self, data, current_year):
        """Calculating the amount of materials and environmental stressors
        generated in the use phase of the ship. There are four different parts
        of the use phase: General processes that happens throughout the life
        of the ship and three operational profiles - cruise, manoeuvre, hotel
            1. Use. Flows. Materials needed for maintenance and handled
               throughout the lifetime of the ship (bilge oil). Use of
               port facilities are also included here.
            2. Cruise. Stressors. When the ship runs at design speed, which
               is counted as 70 % main engine load. (This is most of the time.)
            3. Manoeuvre. Stressors. When the ship runs more slowly due to e.g.
               being close to shore. 25 % main engine load.
            4. Hotel. Stressors. When the ship engines are not running, and it
               is shored up. (0 % main engine load.)
        For the operational lifecycles the stressors are calculated differently
        dependent on whether they are fuel-specific or engine-specific. I have
        followed the 'EMEP/EEA air pollutant emission inventory guidebook'
        published in 2016.
        """
        tkm_year = self.tkm
        port_char = self.port_char
        eco_bulker = data.ecoinv.PerMisc
        ardaid_df = data.ardaid
        built_year = self.built
        trip_phases_hours_ls = [self.cru_h, self.man_h, self.hot_h]
        slow_number = data.tkminfo[current_year].xs(self.size_bucket).slow_steaming
        main_engine_kW = self.eng_m
        aux_engine_kW = self.eng_a
        t1 = data.ef_tr1.Value_adj
        t3 = data.ef_tr3
        input_list_F = [ardaid_df, 
                        built_year, 
                        current_year, 
                        trip_phases_hours_ls, 
                        slow_number, 
                        tkm_year, 
                        main_engine_kW,
                        aux_engine_kW]
        # A matrix
        self.maintenance = hf.flowdict(tkm_year, 'maint', eco_bulker, ardaid_df)
        self.bilge_oil = hf.flowdict(tkm_year, 'bilge', eco_bulker, ardaid_df)
        self.port = hf.flowdict(port_char, 'port', eco_bulker, ardaid_df)
        # F matrix
        self.nox_air = hf.flowdict_use('NOx', input_list_F, tier=3, efct_df=t3)
        self.nmvoc_air = hf.flowdict_use('NMVOC', input_list_F, tier=3, efct_df=t3)
        self.tsp_air = hf.flowdict_use('PMs_10', input_list_F, tier=3, efct_df=t3)
        self.pm10_air = hf.flowdict_use('PMs_5', input_list_F, tier=3, efct_df=t3)
        self.pm25_air = hf.flowdict_use('PMs_25', input_list_F, tier=3, efct_df=t3)
        self.co2_air = hf.flowdict_use('CO2', input_list_F, tier='CO2')
        self.sox_air = hf.flowdict_use('SOx', input_list_F, tier=1, efct_df=t1)
        self.pb_air = hf.flowdict_use('Pb', input_list_F, tier=1, efct_df=t1)
        self.cd_air = hf.flowdict_use('Cd', input_list_F, tier=1, efct_df=t1)
        self.hg_air = hf.flowdict_use('Hg', input_list_F, tier=1, efct_df=t1)
        self.as_air = hf.flowdict_use('As', input_list_F, tier=1, efct_df=t1)
        self.cr_air = hf.flowdict_use('Cr', input_list_F, tier=1, efct_df=t1)
        self.cu_air = hf.flowdict_use('Cu', input_list_F, tier=1, efct_df=t1)
        self.ni_air = hf.flowdict_use('Ni', input_list_F, tier=1, efct_df=t1)
        self.se_air = hf.flowdict_use('Se', input_list_F, tier=1, efct_df=t1)
        self.zn_air = hf.flowdict_use('Zn', input_list_F, tier=1, efct_df=t1)
        self.pcb_air_use = hf.flowdict_use('PCB', input_list_F, tier=1, efct_df=t1)
        self.pcddf_air  = hf.flowdict_use('PCDD/F', input_list_F, tier=1, efct_df=t1)
        self.hcb_air = hf.flowdict_use('HCB', input_list_F, tier=1, efct_df=t1)
        self.co_air = hf.flowdict_use('CO_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.nh3_air = hf.flowdict_use('NH3_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.pah_air = hf.flowdict_use('PAH_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.benzene_air = hf.flowdict_use('benzene_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.hcl_air = hf.flowdict_use('HCl_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.ch4_air = hf.flowdict_use('CH4_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.toluene_air = hf.flowdict_use('toluene_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.hf_air = hf.flowdict_use('HF_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.xylene_air = hf.flowdict_use('xylene_a', input_list_F, tier=0, efct_df=eco_bulker)
        self.n2o_air = hf.flowdict_use('N2O_a', input_list_F, tier=0, efct_df=eco_bulker)
    
    def transport_to_dict(self):
        return {
            'IMO': self.imo,
            'tkm': self.tkm,
            'bucket': self.size_bucket
        }