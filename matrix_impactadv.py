import os
import re
import copy
import pandas as pd
import matplotlib.pyplot as plt
from scipy import sparse

import class_readdata as rd
import matrix_calculation as matcal
import matrix_support as matsup
import matrix_y as maty
import scenarios_buckets as scebuc

DPI = 600

def future_fleet():
    folders = matsup.get_folder_structure()
    
    seaweb = rd.ReadData(folders['model']).seaweb
    
    y_all = matsup.pickle_load(folders['arda'] + 'y_ALL.dat')
    
    ssps = ['SSP1', 'SSP2', 'SSP3', 'SSP4', 'SSP5']
    
    size_names = scebuc.get_size_buckets_names()
    years = [2016, 2020, 2025, 2030, 2035, 2040, 2045, 2050]
    multidx = pd.MultiIndex.from_product([size_names, years], names=['bucket', 'year'])
    
    df_org = pd.DataFrame(columns=multidx)
    y_sixteen = y_all['2016']
    
    for bucket in size_names:
        y_sub = y_sixteen[(seaweb.bucket == bucket) & (y_sixteen > 0)]
        matrices = import_matrices(folders['arda'], 2016, y_sub)
        d_adv = matcal.advanced_contribution_analysis(matrices)
        df_org[bucket, 2016] = d_adv.sum(axis=1)
    
    for ssp in ssps:
        df_tot = copy.deepcopy(df_org)
        for year in years[1:]:
            y = y_all[ssp + '_' + str(year)]
            for bucket in size_names:
                y_sub = y[(seaweb.bucket == bucket) & (y > 0)]
                matrices = import_matrices(folders['arda'], year, y_sub)
                d_adv = matcal.advanced_contribution_analysis(matrices)
                df_tot[bucket, year] = d_adv.sum(axis=1)
        df_tot.to_pickle(folders['results'] + ssp)
        df_tot.to_excel(folders['results'] + ssp + '.xlsx', sheet_name=ssp)
    
    os.chdir(folders['main'])

def todays_fleet():
    folders = matsup.get_folder_structure()
    
    seaweb = rd.ReadData(folders['model']).seaweb
    size_buckets = scebuc.get_size_buckets()
    
    impact_dfs_list = []
    size_name_list = []
    
    for i in range(len(size_buckets)-1):
        level_subset = seaweb[(seaweb.TEU >= size_buckets[i]) & 
                              (seaweb.TEU < size_buckets[i+1])]
        matrix_list = import_matrices(folders['arda'], level_subset.index.tolist())
        d_adv_df = matcal.advanced_contribution_analysis(matrix_list)
        advanced_contribution_plot(d_adv_df, folders['figs'], 'bucket_'+str(i))
        impact_dfs_list.append(d_adv_df)
        size_name_list.append('{} TEU to {} TEU'.format(size_buckets[i], size_buckets[i+1]))
    
    impact_totals_plot(impact_dfs_list, size_name_list, folders['figs'], 'test')
    os.chdir(folders['main'])


def import_matrices(path, year, y_sub):
    """Imports the matrices needed for the advanced contribution analysis and
    returns a list of them in the right order to be fed to the analysis.
    """
    os.chdir(path)
    
    A_bf = sparse.load_npz(str(year) + '_A_bf.npz')
    L_ff = sparse.load_npz(str(year) + '_L_ff.npz')
    L_bb = sparse.load_npz('L_bb.npz')
    
    F_f = sparse.load_npz(str(year) + '_F_f.npz')
    F_b = sparse.load_npz('F_b.npz')
    
    C = sparse.load_npz('C_matrix.npz')
    C_rc = matsup.pickle_load('C_rowscols.dat')
    
    A_f_idx = matsup.pickle_load(str(year) + '_A_f_rows.dat')
    
    y_f = maty.generate_y_vector(A_f_idx, y_sub)
    
    return [A_bf, L_ff, L_bb, F_f, F_b, C, y_f, C_rc[0]]

def advanced_contribution_plot(d_adv_df, path, name):
    """Takes a data frame with results from advanced contribution analysis, 
    normalises them and plots the results with the total absolute values in
    the labels.
    """
    
    totals_s = d_adv_df.sum(axis=1)
    d_rel_df = d_adv_df.div(totals_s, axis=0) * 100
    
    reindexing = get_reindexing_list(reverse=True)
    df_toplot = d_rel_df.reindex(reindexing)
    
    new_index = []
    for old_name in reindexing:
        split = re.split('/', old_name)
        new_name = split[0] + ': ' + '{:.2E} '.format(totals_s[old_name]) + split[-1]
        new_index.append(new_name)
    
    df_toplot.index = new_index
    df_toplot.columns = get_foreground_names_plot()
    
    df_toplot.plot(kind='barh', stacked=True).legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlim(xmin=0, xmax=100)
    plt.xlabel('Relative impact (%)')
    plt.title(name)
    plt.savefig(path + name + '.png', dpi=DPI, bbox_inches='tight')

def impact_totals_plot(list_of_impact_dfs, names, path, name):
    """Takes a list of data frames with advanced impact results and a list of
    equal length with the names of the data frame contents for the legend.
    Sums the data frames to a single impact vector and plots the results in a
    grouped bar chart.
    """
    impact_totals_list = []
    
    for df in list_of_impact_dfs:
        impact_s = df.sum(axis=1)
        impact_totals_list.append(impact_s)
    
    imptot_df = pd.concat(impact_totals_list, axis=1)
    imptot_rel = imptot_df.div(imptot_df.sum(axis=1), axis=0) * 100
    imptot_rel.columns = names
    
    reindexing = get_reindexing_list()
    imptot_toplot = imptot_rel.reindex(reindexing)
    
    imptot_toplot.plot.bar()
    plt.savefig(path + name + '.png', dpi=DPI)

def get_reindexing_list(reverse=False):
    """Selects the impact names I want to show in the plots. Reverse order is
    needed for horizontal stacked bar charts (advanced contribution analysis).
    """
    reindexing_list = ['ULOP100/Hierarchist/m2a', 
                       'ALOP100/Hierarchist/m2a', 
                       'FETP_H/Hierarchist/kg 1,4-DB eq', 
                       'METP_H/Hierarchist/kg 1,4-DB eq', 
                       'HTP_H/Hierarchist/kg 1,4-DB eq', 
                       'MDP100/Hierarchist/kg Fe eq', 
                       'FDP100/Hierarchist/kg oil eq', 
                       'TAP100/Hierarchist/kg SO2 eq', 
                       'POFP100/Hierarchist/kg NMVOC', 
                       'PMFP100/Hierarchist/kg PM10 eq', 
                       'GWP100/Hierarchist/kg CO2 eq']
    return reindexing_list[::-1] if reverse==False else reindexing_list

def get_foreground_names_plot():
    return ['Construction', 
            'Operation, Propulsion',
            'Operation, Other', 
            'End-of-Life', 
            'Fuel']

if __name__ == '__main__':
    future_fleet()