""" Functions to do the life cycle calculations in class_container.py """

def findex(name, df, col='Short_name'):
    """ Returns the ArdaID of a substance based on it's short name """
    ind = df[df[col] == name].index
    return ind[0]


def flowdict(char, flow_name, flow_ser, arda_df, col='Matrix'):
    """ Calculates the flow for the ship and creates a dictionary:
        {'aid':ardaID, 'val':flow value, 'mat':A or F matrix} """
    aid = findex(flow_name, arda_df)
    val = char * flow_ser[aid]
    mat = arda_df.loc[aid, col]
    return {'aid':aid, 'val':val, 'mat':mat}

def flowdict_use(name, input_list, tier, efct_df=None, co2=3114.4):
    """ Calculate the emission for the ship and creates a dictionary: 
        {'aid':ardaID, 'val':value, 'mat':'F'} """
    arda_df = input_list[0]
    built = input_list[1]
    current_year = input_list[2]
    time = input_list[3]
    slow = input_list[4]
    tkm = input_list[5]
    mcr = input_list[6]
    aux = input_list[7]
    
    aid = findex(name, arda_df)
    
    cons_main_cru = fuelcons_main(time[0], 'cru', built, 'main', mcr, slow)
    cons_main_man = fuelcons_main(time[1], 'man', built, 'main', mcr)
    cons_main_hot = fuelcons_main(time[2], 'hot', built, 'main', mcr)
    cons_aux_cru = fuelcons_aux(time[0], 'cru', built, 'aux', aux)
    cons_aux_man = fuelcons_aux(time[1], 'man', built, 'aux', aux)
    cons_aux_hot = fuelcons_aux(time[2], 'hot', built, 'aux', aux)
    
    cons_tot = total_fuelcons(time, built, mcr, aux, slow)
    
    if tier == 'CO2':
        val = cons_tot * co2
    
    if tier == 0:
        val = tkm * efct_df[aid]
    
    if tier == 1:
        if name == 'SOx':
            val = cons_tot * efct_df[name] * get_sox_content_of_fuel(current_year)
        else:
            val = cons_tot * efct_df[name]
    
    if tier == 3:
        if name == 'NOx':
            name = 'NOx_2000'
        if name.startswith('PMs'):
            name = name[:3]
        # Cruise
        cru_main = cons_main_cru * efct_df.loc['Cruise', name]
        cru_aux = cons_aux_cru * efct_df.loc['All', name]
        # Manoeuvre
        man_main = cons_main_man * efct_df.loc['Rest', name]
        man_aux = cons_aux_man * efct_df.loc['All', name]
        # Hotel
        hot_main = cons_main_hot * efct_df.loc['Rest', name]
        hot_aux = cons_aux_hot * efct_df.loc['All', name]
        # Total
        val = sum([cru_main, cru_aux, man_main, man_aux, hot_main, hot_aux])
        if (name == 'NOx_2000') & (built >= 2000):
            val *= (1 - 0.17)
        if (name == 'NOx_2000') & (built >= 2011):
            val *= (1 - 0.15)
        if name == 'PMs':
            val /= 3
    
    return {'aid':aid, 'val':val, 'mat':'F'}

def total_fuelcons(time_ls, built, main_mcr, aux_max, slow):
    """ Sums fuel consumption for all three trip phases and both engines, still
    given in tonnes. The time spent in each trip phase is provided as a list of
    the time in hours in the following order: Cruise, manoeuvre, hotel.
    """
    main_cru = fuelcons_main(time_ls[0], 'cru', built, 'main', main_mcr, slow)
    main_man = fuelcons_main(time_ls[1], 'man', built, 'main', main_mcr)
    main_hot = fuelcons_main(time_ls[2], 'hot', built, 'main', main_mcr)
    aux_cru = fuelcons_aux(time_ls[0], 'cru', built, 'aux', aux_max)
    aux_man = fuelcons_aux(time_ls[1], 'man', built, 'aux', aux_max)
    aux_hot = fuelcons_aux(time_ls[2], 'hot', built, 'aux', aux_max)
    return sum([main_cru, main_man, main_hot, aux_cru, aux_man, aux_hot])

def fuelcons_main(time, phase, built, engine, mcr_main, slowsteam=1):
    """ Fuel consumption in tonnes for a specific trip phase and engine type: 
    Time in hours, MCR in kW, SFOC in g/kWh. The name of the trip phase must 
    be 'cru', 'man' or 'hot'. The consumption is converted to tonnes.
    """
    sfoc = sfoc_increasing_with_age(built)
    load = load_factors(phase)
    power = mcr_main * load
    if phase == 'cru':
        slowsteam += 0.25
        if slowsteam > 1:
            print('Slow steam factor too large: ', slowsteam)
    cons = time * power * sfoc * slowsteam * 10**-6
    return cons

def fuelcons_aux(time, phase, built, engine, aux_max):
    sfoc = sfoc_increasing_with_age(built)
    power = mcr_aux(phase)
    if power > aux_max:
        power = aux_max
    cons = time * power * sfoc * 10**-6
    return cons

def load_factors(phase):
    """ Predefined load factors for each trip phase """
    return {
        'cru': 0.80,
        'man': 0.25,
        'hot': 0
    }[phase]

def mcr_aux(phase):
    """ Preset kW needed of auxiliary engine power for each ship phase 
        Source: Jalkanen et al. (2012) Extension of an assessment model """
    return {
        'cru': 750,
        'man': 1250,
        'hot': 1000
    }[phase]

def sfoc_increasing_with_age(built_yr):
    """The specific fuel oil consumption (SFOC) for ships built in different
    decades. The values are a bit arbitrary.
    """
    decade = built_yr - (built_yr % 10)
    return{
        1980:205, 
        1990:195, 
        2000:185, 
        2010:180,
        2020:175, 
        2030:170, 
        2040:165, 
        2050:155
    }[decade]

def get_sox_content_of_fuel(current_year):
    """IMO Regulation 14 on sulphur oxides tighten from 3.50% m/m to 0.50% m/m
    from 1st January 2020.
    """
    return 0.005 if current_year >= 2020 else 0.035