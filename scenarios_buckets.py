# -*- coding: utf-8 -*-
"""
Created on Sun May 21 10:31:32 2017

@author: annar
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import matrix_support as matsup

DPI = 600

def create_future_transport_work():
    folders = matsup.get_folder_structure()
    os.chdir(folders['model'])
    lindstad = pd.read_csv('lindstad_time.csv', skiprows=9, index_col=0, usecols=range(12))

    load_org = lindstad.Utilisation.tolist()[::-1]
    trip_count_org = lindstad.Voyage_count.tolist()[::-1]
    trip_km_org = lindstad.Voyage_km.tolist()[::-1]

    for ls in [load_org, trip_count_org, trip_km_org]:
        ls.insert(0, ls[0])
        ls.append(ls[-1])

    size_names = get_size_buckets_names()
    years = range(2016,2051)

    load = pd.DataFrame(index=size_names, columns=years, dtype='float64')
    trip_count = pd.DataFrame(index=size_names, columns=years, dtype='float64')
    trip_km = pd.DataFrame(index=size_names, columns=years, dtype='float64')

    slow_end_yr = 2031
    init_cond = {'load':0.60,               # load for all ships is 60%
                 'trip_rdct':0.667,         # the speed is reduces by 1/3
                 'size_level':'2300_4000'}  # for ships over 2300 TEU

    load.loc[:,2016] = init_cond['load']
    load.loc[:,slow_end_yr] = load_org
    load.loc[:,:slow_end_yr] = load.loc[:,:slow_end_yr].interpolate(axis=1, method='linear')
    load.fillna(method='ffill', axis=1, inplace=True)

    from_size = init_cond['size_level']
    trip_count.loc[:,2016] = trip_count_org
    trip_count.loc[from_size:,2016] = trip_count.loc[from_size:,2016] * init_cond['trip_rdct']
    trip_count.loc[:,slow_end_yr] = trip_count_org
    trip_count.loc[:,:slow_end_yr] = trip_count.loc[:,:slow_end_yr].interpolate(axis=1, method='linear')
    trip_count.fillna(method='ffill', axis=1, inplace=True)

    trip_km.loc[:,2016] = trip_km_org
    trip_km.fillna(method='ffill', axis=1, inplace=True)

    count_percent = trip_count.div(trip_count_org, axis=0)
    func = lambda x: ((x-init_cond['trip_rdct']) * 0.75 / (1-init_cond['trip_rdct']))   # specific to adjustments and fuel reduction data
    slow_steaming = func(count_percent)
    
    data = {'load':load,
            'trip_count':trip_count,
            'trip_km':trip_km,
            'slow_steaming':slow_steaming}
    panel = pd.Panel.from_dict(data, orient='minor')
    tkm_info_df = panel.to_frame()

    return tkm_info_df

def create_future_segments(figure=False):
    """Set first and final year, and the chosen bucket division for these years.
    Returns a data frame with bucket divisions for each year with percentage of
    newbuilds.
    """
    base_year = 2016
    end_year = 2050
    year_cols = range(base_year+1, end_year+1)   # from the year after base year

    size_buckets = get_size_buckets()
    bucket_index = range(len(size_buckets)-1)

    first_year_dist = [2, 5, 10, 10, 10, 10, 8, 25, 20]
    last_year_dist = [2, 5, 8, 7, 5, 5, 8, 35, 25]

    dist_df = pd.DataFrame(index=bucket_index, columns=year_cols)
    dist_df[base_year+1] = first_year_dist
    dist_df[end_year] = last_year_dist

    for i in bucket_index:
        bucket_row = dist_df.loc[i]

        if bucket_row[base_year+1] == bucket_row[end_year]:
            dist_df.loc[i,:] = bucket_row[base_year+1]
        else:
            x = [base_year+1, end_year]
            y = [bucket_row[base_year+1], bucket_row[end_year]]
            fit = np.polyfit(x, y, 1)
            dist_df.loc[i,:] = np.polyval(fit, bucket_row.index)

    dist_df.index = get_size_buckets_names()

    if figure == True:
        dist_df.index = get_size_buckets_plot_names()
        dist_df.T.plot().legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.xlabel('Year')
        plt.ylabel('Percent of fleet')
        plt.savefig(matsup.get_folder_structure()['figs'] + 'future_buckets.png', dpi=DPI, bbox_inches='tight')

    return dist_df/100

def plot_historic_quantity_and_capacity():
    folders = matsup.get_folder_structure()
    seaweb = import_seaweb_data(folders['model'])

    year = []
    quant = []
    cap = []

    for i in range(2000,2017):
        year.append(i)
        quant.append(seaweb[seaweb.Built == i].count())
        cap.append(seaweb.DWT[seaweb.Built == i].sum())

    plt.plot(year, cap)
    plt.show()

def size_and_year_segments(seaweb):
    """Create DataFrame with mean and standard deviation values for the ships
    in a given segment of size (TEU) and year.
    """

    size_buckets = get_size_buckets()
    years = range(2000, 2017)
    cols = ['year', 'bucket', 'quantity', 'percentage', 'teu_mean', 'teu_std',
            'dwt_mean', 'dwt_std', 'gt_mean', 'gt_std', 'ldt_mean', 'ldt_std',
            'eng_mean', 'eng_std']

    history_df = pd.DataFrame(index=years, columns=cols[1:])
    full_df = pd.DataFrame(columns=cols[1:])

    size_buckets = get_size_buckets_names()

    for size in size_buckets:
        level_subset = seaweb[seaweb.bucket == size]

        for the_year in years:
            total_year = seaweb[seaweb.Built == the_year].DWT.count()
            subset = level_subset[level_subset.Built == the_year]
            bucket = size
            quantity = subset.Name.count()
            percentage = quantity*100 / total_year
            teu_mean = subset.TEU.mean()
            teu_std = subset.TEU.std()
            dwt_mean = subset.DWT.mean()
            dwt_std = subset.DWT.std()
            gt_mean = subset.GT.mean()
            gt_std = subset.GT.std()
            ldt_mean = subset.LDT.mean()
            ldt_std = subset.LDT.std()
            eng_mean = subset.MainEng.mean()
            eng_std = subset.MainEng.std()
            row = [bucket, quantity, percentage, teu_mean, teu_std, dwt_mean, dwt_std,
                   gt_mean, gt_std, ldt_mean, ldt_std, eng_mean, eng_std]
            history_df.loc[the_year] = row

        full_df = full_df.append(history_df)

    return full_df

def get_size_buckets_plot_names():
    sizes = get_size_buckets()

    size_bucket_plot_names = []

    for i in range(len(sizes)-1):
        size_bucket_plot_names.append('{} to {} TEU'.format(sizes[i], sizes[i+1]))

    return size_bucket_plot_names

def get_size_buckets_names():
    sizes = get_size_buckets()

    size_bucket_names = []

    for i in range(len(sizes)-1):
        size_bucket_names.append('{}_{}'.format(sizes[i], sizes[i+1]))

    return size_bucket_names

def get_size_buckets():
    """Yields the size buckets used in Lindstad et al. (2013), with a minor
    modification of dividing the largest bucket (8500+ TEU) into two, with
    a new division line at 12500 TEU.
    """
    return [0, 200, 700, 1400, 2300, 4000, 6500, 8500, 12500, 20000]

def import_seaweb_data(path):
    """Import the Sea-web data used in the analysis, where all values are
    present. Returns pandas DataFrame with IMO no. as index column.
    """
    os.chdir(path)
    filename = 'seaweb.csv'
    return pd.read_csv(filename, index_col=0)


if __name__ == '__main__':
    create_future_transport_work()