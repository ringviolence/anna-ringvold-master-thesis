import os
import time
from scipy import sparse

import matrix_support as matsup

def main():
    start = time.time()
    load_and_save_background_matrices()
    load_invert_and_save_background_A_matrix()
    end = time.time()
    print(end - start)

def load_and_save_background_matrices():
    folders = matsup.get_folder_structure()
    os.chdir(folders['arda'])
    
    A_bb, F_b, C = matsup.get_ecoinvent_background_matrices(folders['arda'])
    
    # A matrix
    A_bb_sparse = sparse.csc_matrix(A_bb.values)
    sparse.save_npz('A_bb.npz', A_bb_sparse)
    matsup.pickle_dump('A_b_rows.dat', list(A_bb.index.values))
    
    # F matrix
    F_b_sparse = sparse.csc_matrix(F_b.values)
    sparse.save_npz('F_b.npz', F_b_sparse)
    matsup.pickle_dump('F_b_rows.dat', list(F_b.index.values))
    
    # C matrix
    C_sparse = sparse.csc_matrix(C.values)
    sparse.save_npz('C_matrix.npz', C_sparse)
    C_rc = [list(C.index.values), list(C.columns.values)]
    matsup.pickle_dump('C_rowscols.dat', C_rc)
    
    os.chdir(folders['main'])

def load_invert_and_save_background_A_matrix():
    folders = matsup.get_folder_structure()
    os.chdir(folders['arda'])
    
    A_bb = sparse.load_npz('A_bb.npz')
    
    b_size = A_bb.shape[0]
    
    eye_b = sparse.identity(b_size, format='csc')
    L_bb = sparse.linalg.inv(eye_b - A_bb)
    
    sparse.save_npz('L_bb.npz', L_bb)
    
    os.chdir(folders['main'])


if __name__ == '__main__':
    main()