"""Takes in the historic data in the 'Scenario' folder and trains three
different regression models: Linear, quadratic and logarithmic. The default
output are the coefficients for the linear model.
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import matrix_support as matsup

def get_regression_coefficients(loadpath, coeff='linear', figure=False):
    folders = matsup.get_folder_structure()
    os.chdir(folders['scenarios'])
    
    gdp, pop, tkm = import_scenario_training_data(loadpath)
    gdp_fut, pop_fut = import_scenario_future_data(loadpath, 'training')
    
    mask_all = gdp.notnull() & pop.notnull() & tkm.notnull()
    mask_list = mask_all[mask_all].index.tolist()
    
    gdp_00_10 = fill_values_ssp_data(gdp, mask_list[0], mask_list[-1])
    pop_00_10 = fill_values_ssp_data(pop, mask_list[0], mask_list[-1])
    
    gdp_10_20 = fill_values_ssp_data(gdp_fut, 2010, 2020)
    pop_10_20 = fill_values_ssp_data(pop_fut, 2010, 2020)
    
    gdp_per_cap_00_10 = gdp_00_10 * 1000 / pop_00_10   # unit is now US$2005/capita
    gdp_per_cap_10_20 = gdp_10_20 * 1000 / pop_10_20
    gdp_per_cap = gdp_per_cap_00_10.append(gdp_per_cap_10_20)
    
    mask_tkm = tkm.notnull() & gdp_per_cap.notnull()
    
    tkm_00_16 = tkm[mask_tkm[mask_tkm].index]
    
    if figure == True:
        scenario_regressions_figure(gdp_per_cap[mask_tkm], tkm_00_16)
    
#    import scipy.stats as st
#    slope, intercept, r_value, p_value, std_err = st.linregress(gdp_per_cap[mask_tkm], tkm_00_16)
#    print(r_value)
    
    lin_coeff = np.polyfit(gdp_per_cap[mask_tkm], tkm_00_16, 1)
    quad_coeff = np.polyfit(gdp_per_cap[mask_tkm], tkm_00_16, 2)
    log_coeff = np.polyfit(np.log(gdp_per_cap[mask_tkm]), tkm_00_16, 1)
    
    os.chdir(folders['main'])
    
    return {'linear':lin_coeff, 
            'quadratic':quad_coeff, 
            'logarithmic':log_coeff, 
            'tkm_data':tkm
            }[coeff]
    
    
def import_scenario_training_data(loadpath):
    """Importing the different data as pandas Series, and calculating 
    tkm values from tmi data.
    """
    
    # Gross World Product (historic), billion US$2005/yr (SSP, World Bank)
    # 1980-2010, five year intervals plus 2008
    gdp_filename = loadpath + 'SSP_GDP_historic.xlsx'
    gdp_past = pd.read_excel(gdp_filename, skiprows=1, skip_footer=1, 
                        parse_cols=np.arange(5,17)).transpose()[0]
    
    # Population (historic), million (SSP, World Bank)
    # 1960-2010, five year intervals plus 2008
    pop_filename = loadpath + 'SSP_Population_historic.xlsx'
    pop_past = pd.read_excel(pop_filename, skiprows=1, skip_footer=1, 
                             parse_cols=np.arange(5,17)).transpose()[0]
    
    # Transport work (historic), estimated billion ton miles (UNCTAD, RMT)
    # 2000-2016, annually
    tmi_filename = loadpath + 'RMT16_tmi_historic.csv'
    tmi_past = pd.read_csv(tmi_filename, index_col=0, skiprows=5)['Containers']
    
    # Converting ton-miles to ton-kilometres (the tonnage in RMT is already metric)
    conv = 1.609344   # kilometres per mile
    tkm_past = tmi_past * conv * 10**9   # converting from billion tkm to tkm
    
    return gdp_past, pop_past, tkm_past

def import_scenario_future_data(loadpath, use='prediction'):
    """Importing the different data as pandas Series, and calculating 
    tkm values from tmi data. UPDATE DESCRIPTION
    """
    # Gross World Product (future), billion US$2005/yr (SSP, OECD)
    # 2005-2100, ten year intervals plus 2005
    gdp_filename = loadpath + 'SSP_GDP_future.xlsx'
    gdp_future = pd.read_excel(gdp_filename, skip_footer=1, index_col=1).iloc[:,4:15]
    
    # Population (future), million (SSP, IIASA)
    # 2010-2100, ten year intervals plus 2005 (but no values for 2005)
    pop_filename = loadpath + 'SSP_Population_future.xlsx'
    pop_future = pd.read_excel(pop_filename, skip_footer=1, index_col=1).iloc[:,4:15]
    
    # Combining to prediction of future GDP per capita
    gdp_per_cap = gdp_future * 1000 / pop_future
    
    return {'training':(gdp_future.mean(0), pop_future.mean(0)), 
            'prediction':gdp_per_cap.dropna(1)   # removes 2005 column
            }[use]

def fill_values_ssp_data(data_series, start_yr, end_yr):
    """Performing linear regressions to fill in the values for the years
    between 2000 and 2010 in the GDP and population data sets.
    """
    index_s = pd.Series(data_series.index)
    index_mask = index_s.between(start_yr, end_yr)
    res_s = pd.Series([])
    for i, j in zip(*[index_s[index_mask].values[k:] for k in range(2)]):
        res_s = res_s.append(pd.Series(data_series[i], index=[i]))
        x = [i, j]
        y = [data_series[i], data_series[j]]
        linear = np.polyfit(x, y, 1)
        for m in range(i+1, j):
            new_entry = pd.Series(np.polyval(linear, m), index=[m])
            res_s = res_s.append(new_entry)
    res_s = res_s.append(pd.Series(data_series[end_yr], index=[end_yr]))
    return res_s

def scenario_regressions_figure(pred, resp, save=False):
    """Linear, quadratic and logarithmic regression betweeen predictor and
    response variable. The input must be series with matching indices.
    """
    linear = np.polyfit(pred, resp, 1)
    quadratic = np.polyfit(pred, resp, 2)
    logarithmic = np.polyfit(np.log(pred), resp, 1)
    
    # Creating an x-axis to plot the functions against
    x = np.linspace(np.amin(pred), np.amax(pred)*2)
    
    # Overlaying a scatter plot of the data on the relationship functions
    _ = plt.plot(x, np.polyval(linear, x), 'b', label='Linear')
    _ = plt.plot(x, np.polyval(quadratic, x), 'g', label='Quadratic')
    _ = plt.plot(x, np.polyval(logarithmic, np.log(x)), 'r', label='Logarithmic')
    _ = plt.plot(pred, resp, 'k', marker='.', linestyle='none')
    
    # Adding axis labels and legend
    _ = plt.xlabel('GDP per capita (US$2005/cap)')
    _ = plt.ylabel('Transport work (estimated ton-kilometres)')
    _ = plt.legend()
        
    if save == True:
        folders = matsup.get_folder_structure()
        with sns.plotting_context('notebook', font_scale=2.5):
            plt.savefig(folders['figs'] + 'scenario_GDPpercap_tkm.png', dpi=400)
        
    return plt.show()


if __name__ == '__main__':
    get_regression_coefficients(matsup.get_folder_structure()['scenarios'])