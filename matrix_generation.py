"""This file gathers all the class and matrix files, generates the full
contribution (A), Leontief (L), stressor (F) and characterization (C) matrices, 
serialises and saves them to the Arda-folder. It takes about 6 minutes to run
on an x64-based PC with 3.40 GHz processor and 16 GB RAM.
"""

import os
from scipy import sparse

import matrix_support as matsup

LIFETIME = 25

def main():
    
    for year in [2016, 2020, 2025, 2030, 2035, 2040, 2045, 2050]:
        generate_and_save_foreground_matrices(year)

def generate_and_save_foreground_matrices(year):
    folder_structure = matsup.get_folder_structure()
    
    ship_list, attributes = matsup.generate_ship_list(2016)
        
    A_bb, F_b, C = matsup.get_ecoinvent_background_matrices(folder_structure['arda'])
    
    # Beginning the matrices and lists that are built upon in the for loop
    A_ff_full, A_ff_rows = matsup.populate_A_ff(ship_list[0], LIFETIME)
    A_bf_full = matsup.populate_A_bf(ship_list[0], A_bb.index, attributes)
    F_f_full = matsup.populate_F_f(ship_list[0], F_b.index, attributes)
    
    for ship in ship_list[1:]:
        # A_ff
        Aff_v, Aff_i = matsup.populate_A_ff(ship, LIFETIME)
        A_ff_rows.extend(Aff_i)
        A_ff_full = sparse.block_diag((A_ff_full, Aff_v), 'csc')
        # A_bf
        Abf_v = matsup.populate_A_bf(ship, A_bb.index, attributes)
        A_bf_full = sparse.hstack((A_bf_full, Abf_v), 'csc')
        # F_f
        Ff_v = matsup.populate_F_f(ship, F_b.index, attributes)
        F_f_full = sparse.hstack((F_f_full, Ff_v), 'csc')
        del ship
    
    # Invert foreground A matrix
    eye_f = sparse.identity(A_ff_full.shape[0], format='csc')
    L_ff = sparse.linalg.inv(eye_f - A_ff_full)
    
    # Save matrices
    os.chdir(folder_structure['arda'])
    
    # A_ff and A_bf
    sparse.save_npz(str(year) + '_A_ff' + '.npz', A_ff_full)
    sparse.save_npz(str(year) + '_A_bf' + '.npz', A_bf_full)
    matsup.pickle_dump(str(year) + '_A_f_rows' + '.dat', A_ff_rows)
    
    # L_ff
    sparse.save_npz(str(year) + '_L_ff' + '.npz', L_ff)
    
    # F_b
    sparse.save_npz(str(year) + '_F_f' + '.npz', F_f_full)
    
    os.chdir(folder_structure['main'])


if __name__ == '__main__':
    main()