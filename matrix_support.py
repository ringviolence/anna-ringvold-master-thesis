""" This file contains the functions needed to run 'matrix_generation.py'

build_ship_list
get_folder_structure
get_attributes
pickle_dump
pickle_load
get_ecoinvent_background_matrices
empty_matrices
populate_A_ff
populate_A_bf
populate_F_f
"""

import os
import pickle
import numpy as np
import pandas as pd
import scipy.io as sio
from scipy import sparse

import class_container as ct
import class_readdata as rd


def generate_ship_list(current_year):
    """ Main function for running the class scripts. Generates list of ship
        objects and returns the list as well as set of attributes. """
    folders = get_folder_structure()
    
    data = rd.ReadData(folders['model'])
    
    attributes = get_attributes(data)
    ships = []
    
    for imo in data.seaweb.index:
        ship = ct.Container(imo, data)
        ship.calc_cnst(data)
        ship.calc_eol(data)
        ship.calc_transport(data, current_year)
        ship.calc_use(data, current_year)
        ships.append(ship)
        del ship
    
    return ships, attributes

def calculate_tkm_sum(ssp_col, current_year, data):
    
    ships = []
    
    for imo in ssp_col.index:
        ship = ct.Container(imo, data)
        ship.calc_transport(data, current_year)
        ships.append(ship)
        del ship
    
    singles = pd.DataFrame([s.transport_to_dict() for s in ships])
    
    return (singles.set_index('IMO').tkm * ssp_col).sum()

def calculate_tkm_for_year(seaweb_index, current_year, data):
    
    ships = []
    
    for imo in seaweb_index:
        ship = ct.Container(imo, data)
        ship.calc_transport(data, current_year)
        ships.append(ship)
        del ship
    
    return pd.DataFrame([s.transport_to_dict() for s in ships]).set_index('bucket').tkm

def get_folder_structure():
    """ Locates the position of file where this function is called and returns
        a dictionary of strings with the paths to the main folders and all
        necessary subfolders. """
    main_folder = os.path.dirname(os.path.abspath(__file__))
    model_data = main_folder + '\\DataTables\\Model\\'
    arda_data = main_folder + '\\DataTables\\Arda\\'
    scenario_data = main_folder + '\\DataTables\\Scenarios\\'
    results = main_folder + '\\DataTables\\Results\\'
    figures = main_folder + '\\DataTables\\Results\\\\Figures\\'
    return {'main':main_folder, 
            'model':model_data,
            'arda':arda_data, 
            'scenarios':scenario_data, 
            'results':results, 
            'figs':figures}

def get_attributes(data):
    """Get the attributes that are created in each trip phase
    The IMO no. is arbitrary, here I've used Maersk Iowa.
    """
    ship_ref = ct.Container(9298686, data)
    ship = ct.Container(9298686, data)
    ship.calc_cnst(data)
    attr_cnst = set(dir(ship)) - set(dir(ship_ref))
    ship.calc_eol(data)
    attr_eol = set(dir(ship)) - set(dir(ship_ref)) - attr_cnst
    ship.calc_transport(data, 2016)
    attr_trns = set(dir(ship)) - set(dir(ship_ref)) - attr_cnst - attr_eol
    ship.calc_use(data, 2016)
    attr_use = set(dir(ship)) - set(dir(ship_ref)) - attr_cnst - attr_eol - attr_trns
    return [attr_cnst, attr_use, attr_eol]

def pickle_dump(filename, obj):
    """ A short function to do a pickle dump and then closing the file """
    output_file = open(filename, 'wb')
    pickle.dump(obj, output_file)
    output_file.close()

def pickle_load(filename):
    """ A short function to load a pickled item and then closing the file """
    input_file = open(filename, 'rb')
    contents = pickle.load(input_file)
    input_file.close()
    return contents

def get_ecoinvent_background_matrices(arda_path):
    """ Reads the .mat file with the Ecoinvent background data and returns
        three pandas data frames: A, F and C matrices correctly indexed.
        Takes about 6 seconds to run. """
    os.chdir(arda_path)
    
    mat_file = 'Ecoinvent32.mat'
    mat = sio.loadmat(mat_file, 
                      variable_names = ['A_gen', 
                                        'C', 
                                        'F_gen', 
                                        'IMP', 
                                        'IMP_header', 
                                        'PRO_gen', 
                                        'PRO_header', 
                                        'STR', 
                                        'STR_header'],
                      matlab_compatible=False)
    
    # Matrices containing numbers
    A = mat['A_gen']            # 12916x12916 (processes)
    C = mat['C']                # 54x25950 (stressor to impact)
    F = mat['F_gen']            # 25950x12916 (process to stressor)
    
    # Matrices containing information about the other matrices
    IMP = mat['IMP']            # 54x8
    PRO = mat['PRO_gen']        # 12916x16
    STR = mat['STR']            # 25950x13
    
#    # Headers for the information matrices
#    IMP_hdr = mat['IMP_header'] # 1x8
#    PRO_hdr = mat['PRO_header'] # 1x16
#    STR_hdr = mat['STR_header'] # 1x13
    
    # Building the background matrices in pandas
    # Processes and stressors are parsed with ardaIDs
    # The impact categories are the names
    imp_id = [i.tolist()[0] for i in IMP[:,0]]
    pro_id = [int(i.tolist()[0][0]) for i in PRO[:,1]]
    str_id = [int(i.tolist()[0][0]) for i in STR[:,1]]
    
    processes = pd.DataFrame(A.todense(), index=pro_id, columns=pro_id)
    stressors = pd.DataFrame(F.todense(), index=str_id, columns=pro_id)
    character = pd.DataFrame(C.todense(), index=imp_id, columns=str_id)
    
    return processes, stressors, character

def empty_matrices(IMO, kind, pro_idx=[1], str_idx=[1]):
    """ Generates empty matrices of the right size to later fill with values
        contained in the ship objects """
    f_flows = get_foreground_flow_names()
    f_names = [str(IMO) + '_' + f for f in f_flows]
    empty_A_ff = pd.DataFrame(np.zeros((5,5)), index=f_names, columns=f_names)
    empty_A_bf = pd.DataFrame(np.zeros((len(pro_idx),5)), index=pro_idx, columns=f_names)
    empty_F_f = pd.DataFrame(np.zeros((len(str_idx),5)), index=str_idx, columns=f_names)
    return {
        'A_ff':empty_A_ff,
        'A_bf':empty_A_bf,
        'F_f':empty_F_f
    }[kind]

def populate_A_ff(ship, lifetime):
    """ Creates an empty A_ff matrix and fills it with values from the ship
        object in the input. Returns index, columns and a sparse csc matrix """
    IMO = ship.imo
    A_ff = empty_matrices(IMO, 'A_ff')
    # Encoding the values for the given ship
    A_ff.iloc[(0,3),2] = 1 / lifetime
    A_ff.iloc[1,2] = 1
    A_ff.iloc[4,2] = ship.fuel * 1000   #convert to kilograms
    return sparse.csc_matrix(A_ff.values), list(A_ff.index.values)

def populate_A_bf(ship, pro_indx, attributes, use='model'):
    """ Creates an empty A_bf matrix and fills it with values from the ship
        object in the input. Returns index, columns and a sparse csc matrix """
    dct = ship.__dict__
    IMO = ship.imo
    A_bf = empty_matrices(IMO, 'A_bf', pro_indx)
    # Encoding the values for the given ship
    for cnst_attr in attributes[0]:
        if dct[cnst_attr]['mat'] == 'A':
            A_bf.loc[dct[cnst_attr]['aid'], str(IMO)+'_cnst'] = dct[cnst_attr]['val']
    for use_attr in attributes[1]:
        if dct[use_attr]['mat'] == 'A':
            A_bf.loc[dct[use_attr]['aid'], str(IMO)+'_use_oth'] = dct[use_attr]['val']
    for eol_attr in attributes[2]:
        if dct[eol_attr]['mat'] == 'A':
            A_bf.loc[dct[eol_attr]['aid'], str(IMO)+'_eol'] = dct[eol_attr]['val']
    # Encoding fuel consumption, which is proportional and the same for each ship
    A_bf.loc[5839, str(IMO)+'_fuel'] = 0.82919876733436
    A_bf.loc[5875, str(IMO)+'_fuel'] = 0.17080123266564
    return {
        'model':sparse.csc_matrix(A_bf.values),
        'testing':(A_bf.index, A_bf.columns, A_bf.values)
    }[use]

def populate_F_f(ship, str_indx, attributes, use='model'):
    """ Creates an empty F_f matrix and fills it with values from the ship
        object in the input. Returns index, columns and a sparse csc matrix """
    dct = ship.__dict__
    IMO = ship.imo
    F_f = empty_matrices(IMO, 'F_f', str_idx=str_indx)
    # Encoding the values for the given ship
    for cnst_attr in attributes[0]:
        if dct[cnst_attr]['mat'] == 'F':
            F_f.loc[dct[cnst_attr]['aid'], str(IMO)+'_cnst'] = dct[cnst_attr]['val']
    for use_attr in attributes[1]:
        if dct[use_attr]['mat'] == 'F':
            F_f.loc[dct[use_attr]['aid'], str(IMO)+'_use_prp'] = dct[use_attr]['val']
    for eol_attr in attributes[2]:
        if dct[eol_attr]['mat'] == 'F':
            F_f.loc[dct[eol_attr]['aid'], str(IMO)+'_eol'] = dct[eol_attr]['val']
    return {
        'model':sparse.csc_matrix(F_f.values),
        'testing':(F_f.index, F_f.columns, F_f.values)
    }[use]

def get_foreground_flow_names():
    """Yields the names of the foreground system as a list.
    """
    return ['cnst', 'use_prp', 'use_oth', 'eol', 'fuel']