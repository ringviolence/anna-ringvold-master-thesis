import os
import copy
import numpy as np
import pandas as pd
from scipy import sparse

import class_readdata as rd
import matrix_support as matsup
import scenarios_future as scefut
import scenarios_buckets as scebuc

def main():
    folders = matsup.get_folder_structure()
    data = rd.ReadData(folders['model'])
    lifetime_normal_distribution(data.seaweb, data, folders['arda'])
    os.chdir(folders['main'])

def generate_y_vector(row_list, series_sub):
    """ Returns y matrix with use phase of ships in given list set to one year """
    name_list = [str(i) + '_use_oth' for i in series_sub.index]
    rows = np.array([])
    for name in name_list:
        rows = np.append(rows, row_list.index(name))
    cols = np.zeros(len(rows))
    data = series_sub.values
    y = sparse.csc_matrix((data, (rows, cols)), shape=(len(row_list),1))
    return y

def lifetime_normal_distribution(seaweb, data, savepath):
    
    LIFETIME = 25   
    STD = 5
    
    start_yr = 2017
    end_yr = 2050
    scrap_rate = 0.03
    
    future_tkm = scefut.get_transport_work_for_future_years(savepath)
    future_dist = scebuc.create_future_segments()
    
    transfer_dict = {}
    for year in np.arange(2017,2051):
        transfer_dict[year] = dict(zip(scebuc.get_size_buckets_names(), 
                     seaweb[seaweb.Built == year].index.tolist()))
    
    seaweb.loc[[isinstance(x,int) for x in seaweb.index], 'y'] = 1
    seaweb.loc[[isinstance(x,str) for x in seaweb.index], 'y'] = 0
    
    ydict = {'2016':copy.deepcopy(seaweb['y'])}
    
    for ssp in future_tkm.index:
        seaweb.loc[[isinstance(x,int) for x in seaweb.index], 'y'] = 1
        seaweb.loc[[isinstance(x,str) for x in seaweb.index], 'y'] = 0
        for current_year in np.arange(start_yr,end_yr+1):
            np.random.seed(current_year)
            norm = np.random.normal(LIFETIME, STD, 10000)   # assuming no more than 10000 tries is needed to fill TEU quota
            built_in = current_year - np.around(norm).astype(int)
            sum_current = (seaweb.TEU * seaweb['y']).sum() * scrap_rate
            TEU_c = seaweb.TEU[seaweb.Built == current_year-45].sum()
            seaweb.loc[seaweb.Built == current_year-45, 'y'] = 0
            for i in built_in:
                subset = seaweb[seaweb.Built == i]
                try:
                    random_index = np.random.choice(subset.index, replace=False)
                    if seaweb.loc[random_index, 'y'] >= 1:
                        seaweb.loc[random_index, 'y'] -= 1
                        TEU_c += subset.TEU[random_index]
                        if TEU_c > sum_current:
                            break
                except ValueError:
                    pass
            tkm_old_ships = matsup.calculate_tkm_sum(seaweb.loc[seaweb['y'] > 0, 'y'], current_year, data)
            tkm_missing = future_tkm.loc[ssp, current_year] - tkm_old_ships
            if tkm_missing > 0:
                current_sub = seaweb[seaweb.Built == current_year]
                tkm_current_indv = matsup.calculate_tkm_for_year(current_sub.index, current_year, data)
                new_ships = solve_linear_system(future_dist[current_year], tkm_current_indv, tkm_missing, current_sub.index)
                seaweb.loc[seaweb.Built == current_year, 'y'] = new_ships
            ydict[ssp + '_' + str(current_year)] = copy.deepcopy(seaweb.y)
    
    os.chdir(savepath)
    matsup.pickle_dump('y_ALL.dat', ydict)
    
    return ydict

def solve_linear_system(percentages, tkm_indv, tkm_missing, idx):
    A_1 = np.identity(9, dtype='float')
    A_2 = np.negative(np.array(percentages.values, dtype='float'))[np.newaxis].T
    A_3 = np.array(tkm_indv.values, dtype='float')
    A_4 = np.zeros(1, dtype='float')
    A = np.vstack((np.hstack((A_1, A_2)), np.hstack((A_3, A_4))))
    b_1 = np.zeros(9, dtype='float')
    b_2 = np.array(tkm_missing, dtype='float')
    b = np.hstack((b_1, b_2))
    x = np.linalg.solve(A, b)
    return pd.Series(x[:9], index=idx)

if __name__ == '__main__':
    main()