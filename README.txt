(c) Copyright 2017 Anna Ringvold

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


The proprietary Ecoinvent 3.2 LCI background database and Sea-web 
containership dataset are not included. Textfile placeholders are 
placed in the folders where they should be: DataTables\Arda and
DataTables\Model, respectively. Upon supplying these datasets the
model can be run.

The scenario results are provided in DataTables\Results and all
figures are provided in DataTables\Results\Figures.

Run the files in the following order (estimated time in paranthesis):

1. matrix_inversion_bb.py (8 min)
2. matrix_y.py (5 min)
3. matrix_generation.py (55 min)
4. matrix_impactadv.py (55 min)

The code was run on an x64-based PC with 3.40 GHz processor and
16 GB RAM in Spyder 3.1.4 running Python 3.6.0 64bits.